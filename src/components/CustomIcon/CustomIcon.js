import React from 'react';
import { Icon } from 'antd';

const CustomIcon = ({ icon, styles, action }) => <Icon className={styles} onClick={action} component={() => <img alt="" src={icon} />} />;

export default CustomIcon;
