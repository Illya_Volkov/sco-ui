import React from 'react';
import PropTypes from 'prop-types';

const withLocalize = (WrappedComponent, translations) => {
    return class extends React.Component {
        constructor(props) {
            super(props);

            const { activeLanguage } = props;

            this.getTranslations(activeLanguage);
        }

        getTranslations = code => translations.setLanguage(code);

        shouldComponentUpdate = nextProps => {
            const { activeLanguage } = this.props;
            const hasActiveLanguageChanged = nextProps.activeLanguage !== activeLanguage;

            if (hasActiveLanguageChanged) {
                this.getTranslations(nextProps.activeLanguage);
                return true;
            }
            return false;
        };

        render() {
            return <WrappedComponent translations={translations} {...this.props} />;
        }
    };
};

withLocalize.propTypes = {
    activeLanguage: PropTypes.string.isRequired
};

export { withLocalize };
