import React from 'react';
import { Modal } from 'antd';
import PropTypes from 'prop-types';

import CustomIcon from 'components/CustomIcon';

export default class CustomModal extends React.Component {
    componentDidMount = () => {
        document.getElementById('root').style.filter = 'blur(5px)';
    };

    componentWillUnmount = () => {
        document.getElementById('root').style.filter = 'unset';
    };

    render() {
        const { onCloseModal, onOkModal, styles, width, height, title, icon, okText, cancelText, closeIcon, bodyText } = this.props;

        return (
            <Modal
                title={title}
                onCancel={onCloseModal}
                onOk={onOkModal}
                width={width}
                height={height}
                className={styles}
                okText={okText}
                cancelText={cancelText}
                visible={true}
                centered={true}
                mask={false}
                maskClosable={false}
            >
                <CustomIcon action={onCloseModal} icon={closeIcon} styles="close-icon" />
                <CustomIcon icon={icon} styles="body-icon" />
                <span className="body-text">{bodyText}</span>
            </Modal>
        );
    }
}

CustomModal.propTypes = {
    onCloseModal: PropTypes.func.isRequired,
    onOkModal: PropTypes.func.isRequired,
    styles: PropTypes.string.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    okText: PropTypes.string.isRequired,
    cancelText: PropTypes.string,
    closeIcon: PropTypes.string,
    bodyText: PropTypes.string
};
