import React from 'react';
import CustomIcon from '../CustomIcon';

const CustomButton = ({ text, icon, styles, action }) => (
    <button className={styles} onClick={action}>
        <CustomIcon icon={icon} />
        <span>{text}</span>
    </button>
);

export default CustomButton;
