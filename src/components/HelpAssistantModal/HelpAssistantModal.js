import React from 'react';
import PropTypes from 'prop-types';
import CustomModal from 'components/CustomModal';

import assistantIcon from 'assets/assistant-large.svg';
import closeRedIcon from 'assets/close-red.svg';

import './HelpAssistantModal.scss';
class HelpAssistantModal extends React.Component {
    render() {
        const { showHelpModal, showAssistantModal, closeHelpModal, openAssistantModal, closeAssistantModal, direction } = this.props;

        return (
            <div>
                {showHelpModal && (
                    <CustomModal
                        width={840}
                        height={580}
                        icon={assistantIcon}
                        closeIcon={closeRedIcon}
                        onCloseModal={closeHelpModal}
                        onOkModal={openAssistantModal}
                        styles={`help-modal help-modal--${direction === 'ltr' ? direction : 'rtl'}`}
                        title="Потрібна допомога асистента?"
                        okText="Так"
                        cancelText="Ні"
                    />
                )}
                {showAssistantModal && (
                    <CustomModal
                        width={540}
                        height={580}
                        icon={assistantIcon}
                        onCloseModal={closeAssistantModal}
                        onOkModal={closeAssistantModal}
                        styles="assistant-modal"
                        title="Вже йду"
                        okText="Добре"
                        bodyText="Будь ласка, зачекайте трохи"
                    />
                )}
            </div>
        );
    }
}

HelpAssistantModal.propTypes = {
    showHelpModal: PropTypes.bool.isRequired,
    showAssistantModal: PropTypes.bool.isRequired,
    closeHelpModal: PropTypes.func.isRequired,
    openAssistantModal: PropTypes.func.isRequired,
    closeAssistantModal: PropTypes.func.isRequired,
    direction: PropTypes.string
};

export default HelpAssistantModal;
