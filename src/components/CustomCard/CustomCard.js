import React from 'react';

const CustomCard = ({ text, img, styles }) => (
    <div className={styles}>
        <img className={`${styles}__img`} alt="" src={img} />
        <span className={`${styles}__text`}>{text}</span>
    </div>
);

export default CustomCard;
