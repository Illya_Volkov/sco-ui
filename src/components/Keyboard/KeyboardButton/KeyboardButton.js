import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import '../Keyboard.scss';

export default class KeyboardButton extends PureComponent {
    static propTypes = {
        value: PropTypes.oneOfType([PropTypes.string.isRequired, PropTypes.node.isRequired]),
        classes: PropTypes.string,
        onClick: PropTypes.func.isRequired,
        autofocus: PropTypes.bool,
        isDisabled: PropTypes.bool
    };

    static defaultProps = {
        classes: '',
        autofocus: false,
        isDisabled: false
    };

    handleClick = () => {
        const { onClick, value } = this.props;

        return onClick(value);
    };

    render() {
        const { classes, isDisabled, autofocus, value } = this.props;
        return (
            <button
                type="button"
                className={`keyboard__button ${classes}`}
                onClick={isDisabled ? null : this.handleClick}
                autoFocus={autofocus}
                disabled={isDisabled}
            >
                {value}
            </button>
        );
    }
}
