import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import KeyboardButton from './KeyboardButton';
import { LatinLayout, CyrillicLayout } from './KeyboardLayouts';
import { BackspaceIcon, LanguageIcon, ShiftIcon, AssistantIcon, HideIcon } from './KeyboardIcons';

import './Keyboard.scss';

// const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

export default class Keyboard extends PureComponent {
    static propTypes = {
        layouts: PropTypes.arrayOf(
            PropTypes.shape({
                layout: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string))
            })
        ),
        inputNode: PropTypes.instanceOf(Element),
        helpAssistant: PropTypes.func.isRequired,
        hideKeyboard: PropTypes.func.isRequired
    };

    static defaultProps = {
        layouts: [CyrillicLayout, LatinLayout]
    };

    state = {
        currentLayout: 0
    };

    handleLanguageClick = () => {
        const { currentLayout } = this.state;
        const { inputNode, layouts } = this.props;

        this.setState({
            currentLayout: (currentLayout + 1) % layouts.length
        });

        inputNode.focus();
    };

    handleShiftClick = () => {
        const { uppercase } = this.state;
        const { inputNode } = this.props;

        this.setState({ uppercase: !uppercase });

        inputNode.focus();
    };

    handleLetterButtonClick = key => {
        const { inputNode } = this.props;
        const { value, selectionStart, selectionEnd } = inputNode;
        const nextValue = value.substring(0, selectionStart) + key + value.substring(selectionEnd);

        inputNode.value = nextValue;

        setTimeout(() => {
            inputNode.focus();
            inputNode.setSelectionRange(selectionStart + 1, selectionStart + 1);
        }, 0);

        inputNode.dispatchEvent(new Event('input', { bubbles: true }));
    };

    handleBackspaceClick = () => {
        const { inputNode } = this.props;
        const { value, selectionStart, selectionEnd } = inputNode;
        let nextValue;
        let nextSelectionPosition;

        if (selectionStart === selectionEnd) {
            nextValue = value.substring(0, selectionStart - 1) + value.substring(selectionEnd);
            nextSelectionPosition = selectionStart - 1;
        } else {
            nextValue = value.substring(0, selectionStart) + value.substring(selectionEnd);
            nextSelectionPosition = selectionStart;
        }

        nextSelectionPosition = nextSelectionPosition > 0 ? nextSelectionPosition : 0;
        inputNode.value = nextValue;

        setTimeout(() => {
            inputNode.focus();
            inputNode.setSelectionRange(nextSelectionPosition, nextSelectionPosition);
        }, 0);

        inputNode.dispatchEvent(new Event('input', { bubbles: true }));
    };

    getKeys() {
        const { layouts } = this.props;
        const { currentLayout, uppercase } = this.state;

        let keysSet = layouts[currentLayout].layout;

        return uppercase ? keysSet.map(keyRow => keyRow.map(key => key.toUpperCase())) : keysSet;
    }

    renderKeyRows() {
        const keys = this.getKeys();

        return keys.map((row, i) => (
            <div className="keyboard__row" key={`row-${i}`}>
                {i === keys.length - 1 && <KeyboardButton value={<ShiftIcon />} classes="keyboard__option-button" onClick={this.handleShiftClick} />}
                {row.map(button => (
                    <KeyboardButton value={button} onClick={this.handleLetterButtonClick} key={button} />
                ))}
                {i === keys.length - 1 && (
                    <div className="keyboard__row">
                        <KeyboardButton value="." onClick={this.handleLetterButtonClick} />
                        <KeyboardButton value={<BackspaceIcon />} classes="keyboard__option-button" onClick={this.handleBackspaceClick} />
                    </div>
                )}
            </div>
        ));
    }

    renderAlphanumeric() {
        const { helpAssistant, hideKeyboard, layouts } = this.props;
        const { currentLayout } = this.state;
        const { optionsKeys } = layouts[currentLayout];

        return (
            <div dir="ltr" className="keyboard">
                {/* <div className="keyboard__row">
                    {numbers.map(button => (
                        <KeyboardButton value={button} onClick={this.handleLetterButtonClick} classes="keyboard__number-button" key={button} />
                    ))}
                </div> */}
                {this.renderKeyRows()}
                <div className="keyboard__row">
                    <KeyboardButton
                        classes="keyboard__assistant-button"
                        value={
                            <AssistantIcon
                                value={optionsKeys.helpButton}
                                textClass="keyboard__assistant-button--text"
                                iconClass="keyboard__assistant-button--icon"
                            />
                        }
                        onClick={helpAssistant}
                    />
                    {layouts.length > 1 ? (
                        <KeyboardButton
                            classes="keyboard__language-button"
                            value={
                                <LanguageIcon
                                    value={optionsKeys.languageButton}
                                    textClass="keyboard__language-button--text"
                                    iconClass="keyboard__language-button--icon"
                                />
                            }
                            onClick={this.handleLanguageClick}
                        />
                    ) : null}
                    <KeyboardButton value={' '} classes="keyboard__space-button" onClick={this.handleLetterButtonClick} />
                    <KeyboardButton
                        classes="keyboard__hide-button"
                        value={<HideIcon value={optionsKeys.hideButton} iconClass="keyboard__hide-button--icon" />}
                        onClick={hideKeyboard}
                    />
                </div>
            </div>
        );
    }

    render() {
        const { inputNode } = this.props;

        if (!inputNode) {
            return null;
        }

        return this.renderAlphanumeric();
    }
}
