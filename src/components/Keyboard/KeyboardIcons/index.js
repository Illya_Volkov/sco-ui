export { default as BackspaceIcon } from './BackspaceIcon';
export { default as LanguageIcon } from './LanguageIcon';
export { default as ShiftIcon } from './ShiftIcon';
export { default as AssistantIcon } from './AssistantIcon';
export { default as HideIcon } from './HideIcon';
