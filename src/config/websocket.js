const host = 'localhost';

const port = 8081;

module.exports = {
    uri: `ws://${host}:${port}`
};
