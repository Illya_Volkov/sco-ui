import LocalizedStrings from 'react-localization';

const translations = new LocalizedStrings({
    en: {
        apiece: {
            title: 'Scan the product'
        },
        weighted: {
            title: 'Product without barcode'
        }
    },
    ua: {
        apiece: {
            title: 'Просканувати товар'
        },
        weighted: {
            title: 'Товар без штрихкоду'
        }
    }
});

export { translations };
