import LocalizedStrings from 'react-localization';

const translations = new LocalizedStrings({
    en: {
        assistant: 'Call an assistant',
        volume: 'Volume'
    },
    ua: {
        assistant: 'Викликати асистента',
        volume: 'Звук'
    }
});

export { translations };
