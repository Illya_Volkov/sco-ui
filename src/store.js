import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from 'data';
import { websocketActions } from 'data/websocket';

const env = process.env.NODE_ENV;
const logger = createLogger({
    collapsed: true
});

const middlewares = [applyMiddleware(thunk, logger)];
const reduxDevTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

if (env === 'development' && reduxDevTools) {
    middlewares.push(reduxDevTools);
}

const configureStore = state => {
    const store = createStore(rootReducer, state, compose(...middlewares));

    websocketActions.init(store);

    return store;
};

export default configureStore;
