import ReconnectingWebSocket from 'reconnecting-websocket';
import { uri } from 'config/websocket.js';

export const UPDATE_STAGE_TITLE = 'UPDATE_STAGE_TITLE';

// const messageTypes = [UPDATE_STAGE_TITLE].reduce((accum, msg) => {
//     accum[msg] = msg;
//     return accum;
// }, {});

export const init = store => {
    const rws = new ReconnectingWebSocket(uri);
    
    rws.addEventListener('open', () => {
        rws.send('open');
    });
    
    rws.onopen = () => {
        console.log('Opened connection ');
    
        // send data to the server
        const json = JSON.stringify({ message: 'Hello ' });
        rws.send(json);
    };
    
    rws.onerror = event => {
        console.log(event);
    };
    
    rws.onclose = (code, reason) => {
        console.log(code, reason);
    };

    rws.onmessage = event => {
        const data = JSON.parse(event.data);
        const {type, payload} = data;
        store.dispatch({ type, payload });
    };

};
