import { UPDATE_STAGE_TITLE } from './websocket.actions';

export const defaultState = {};

export default (state = defaultState, action) => {

    const { type, payload } = action;
    switch (type) {
        case UPDATE_STAGE_TITLE:
            return { ...state, test: { payload } };

        default:
            return state;
    }
};
