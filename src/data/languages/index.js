import languagesReducer from './languages.reducers';
import * as languagesActions from './languages.actions';
import * as languagesSelectors from './languages.selectors';

export { languagesReducer, languagesActions, languagesSelectors };
