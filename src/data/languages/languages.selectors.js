export const getActiveLanguage = state => state.languages.activeLanguage;
export const getLanguages = state => state.languages.languages;
