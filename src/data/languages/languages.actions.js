export const SET_ACTIVE_LANGUAGE = 'SET_ACTIVE_LANGUAGE';
export const SET_LANGUAGES = 'SET_LANGUAGES';

export const setActiveLanguage = language => ({
    type: SET_ACTIVE_LANGUAGE,
    payload: {
        language
    }
});

export const setLanguages = ({ languages }) => ({
    type: SET_LANGUAGES,
    payload: {
        languages
    }
});
