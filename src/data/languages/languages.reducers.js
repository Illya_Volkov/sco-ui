import { SET_ACTIVE_LANGUAGE, SET_LANGUAGES } from './languages.actions';

export const defaultState = {};

export default (state = defaultState, action) => {
    const { type, payload } = action;

    switch (type) {
        case SET_ACTIVE_LANGUAGE:
            return { ...state, activeLanguage: payload.language };

        case SET_LANGUAGES:
            return { ...state, languages: payload };

        default:
            return state;
    }
};
