import { combineReducers } from 'redux';
// import { localizeReducer as localize } from 'react-localize-redux';
import { directionReducer as direction } from './direction';
import { websocketReducer as websocket } from './websocket';
import { languagesReducer as languages } from './languages';

export default combineReducers({ languages, direction, websocket });
