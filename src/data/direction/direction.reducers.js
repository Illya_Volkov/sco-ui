import { SET_DIRECTION } from './direction.actions';

export const defaultState = {
    direction: 'ltr'
};

export default (state = defaultState, action) => {
    const { type, payload } = action;

    switch (type) {
        case SET_DIRECTION:
            return { ...state, direction: payload.direction };

        default:
            return state;
    }
};
