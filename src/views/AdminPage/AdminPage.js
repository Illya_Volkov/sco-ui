// import { Layout } from 'antd';
import React from 'react';

// const { Content } = Layout;

import Keyboard from 'components/Keyboard';
class AdminPage extends React.Component {
    constructor(props) {
        super(props);
        this.inputRef = React.createRef();
    }

    state = {
        value: '',
        inputNode: null
    };

    handleInput = event => {
        const {
            target: { value }
        } = event;
        this.setState({ value });
    };

    helpAssistant = () => alert('ASSISTANT');
    handleFocus = () => this.setState({ inputNode: this.inputRef.current });
    hideKeyboard = () => this.setState({ inputNode: null });

    render() {
        const { inputNode, value } = this.state;
        return (
            <div>
                <input onInput={this.handleInput} onChange={this.handleInput} value={value} onFocus={this.handleFocus} ref={this.inputRef} />
                <Keyboard inputNode={inputNode} helpAssistant={this.helpAssistant} hideKeyboard={this.hideKeyboard} />
            </div>
        );
    }
}

export default AdminPage;
