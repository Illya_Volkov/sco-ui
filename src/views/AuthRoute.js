import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const AuthRoute = ({ component: Component, authenticated, redirectTo = 'main', ...rest }) => {
    return (
        <Route
            {...rest}
            render={props =>
                authenticated ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: redirectTo,
                            state: { from: props.location }
                        }}
                    />
                )
            }
        />
    );
};

export default AuthRoute;
