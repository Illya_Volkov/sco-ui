import React from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import { Layout } from 'antd';
import { connect } from 'react-redux';
import { languagesActions } from 'data/languages';
import AuthRoute from './AuthRoute';
import MainPage from './ClientPage/MainPage';
import TechnicalBreakPage from './ClientPage/TechnicalBreakPage';
import PaymentPage from './ClientPage/PaymentPage';
import AdminPage from './AdminPage';
import ClientPage from './ClientPage';
import WeightedGoods from './ClientPage/WeightedGoods';
import BarcodeGoods from './ClientPage/BarcodeGoods';

import ukrainianIcon from 'assets/flag-icons/ukrainian.svg';
import englishIcon from 'assets/flag-icons/english.svg';
import arabicIcon from 'assets/flag-icons/arabic.svg';

class Index extends React.Component {
    constructor(props) {
        super(props);

        this.initializeLanguages();
        this.setActiveLanguage();
    }

    setActiveLanguage = () => {
        this.props.setActiveLanguage('en');
    };

    initializeLanguages = () => {
        this.props.setLanguages({
            languages: [
                { name: 'Ukrainian', code: 'ua', icon: ukrainianIcon },
                { name: 'English', code: 'en', icon: englishIcon },
                { name: 'Arabic', code: 'ar', icon: arabicIcon }
            ]
        });
    };

    render() {
        return (
            <Layout className="main-layout">
                <Switch>
                    <Route path="/client/main" component={MainPage} />
                    <Route
                        exact
                        path="/client/weighted-goods"
                        component={() => (
                            <ClientPage>
                                <WeightedGoods />
                            </ClientPage>
                        )}
                    />
                    <Route
                        exact
                        path="/client/barcode-goods"
                        component={() => (
                            <ClientPage>
                                <BarcodeGoods />
                            </ClientPage>
                        )}
                    />
                    <Route path="/client/payment" component={PaymentPage} />
                    <Route path="/client/technical-break" component={TechnicalBreakPage} />
                    <AuthRoute path="/admin" authenticated={true} component={AdminPage} />
                    <Redirect from="/" to="/client/main" />
                </Switch>
            </Layout>
        );
    }
}

Index.propTypes = {
    setActiveLanguage: PropTypes.func.isRequired,
    setLanguages: PropTypes.func.isRequired
};

export default withRouter(
    connect(
        undefined,
        languagesActions
    )(Index)
);
