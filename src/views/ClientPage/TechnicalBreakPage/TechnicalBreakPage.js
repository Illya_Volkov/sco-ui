import React from 'react';
import CustomCard from 'components/CustomCard';

import ssoIcon from 'assets/sso.svg';

import './TechnicalBreakPage.scss';

class TechnicalBreakPage extends React.Component {
    render() {
        return (
            <div className="technical-break-page">
                <CustomCard styles="technical-break-page__sso-card" className="test" img={ssoIcon} text="Sorry, we have a technical break" />
            </div>
        );
    }
}

export default TechnicalBreakPage;
