import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Layout } from 'antd';

import Header from './Layout/Header';
import SiderMenu from './Layout/SiderMenu';
import Footer from './Layout/Footer';
import { directionSelectors } from 'data/direction';

import './ClientPage.scss';

const { Content } = Layout;

class ClientPage extends React.Component {
    state = { visible: false };

    render() {
        const { children, direction } = this.props;

        return (
            <Layout dir={direction} className="client-page-layout">
                <Layout>
                    <Header />
                    <Content>{children}</Content>
                </Layout>
                <Footer />
                <SiderMenu />
            </Layout>
        );
    }

    showModal = () => {
        this.setState({
            visible: true
        });
    };
}

ClientPage.propTypes = {
    children: PropTypes.object.isRequired,
    direction: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
    direction: directionSelectors.getDirection(state)
});
export default connect(mapStateToProps)(ClientPage);
