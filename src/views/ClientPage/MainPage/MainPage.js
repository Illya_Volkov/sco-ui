import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Layout } from 'antd';
import PropTypes from 'prop-types';

import Footer from '../Layout/Footer';
import CustomCard from 'components/CustomCard';
import { languagesSelectors } from 'data/languages';
import { directionSelectors } from 'data/direction';
import { translations } from 'translations/mainPage.translations';
import { withLocalize } from 'components/Localize';

import ScannerIcon from 'assets/scanner.svg';
import NoScannerIcon from 'assets/no-scanner.svg';
import VisaIcon from 'assets/visa.svg';
import MastercardIcon from 'assets/mastercard.svg';

import './MainPage.scss';

class MainPage extends React.Component {
    render() {
        const { direction, translations } = this.props;

        return (
            <div dir={direction} className="main-page">
                <Layout dir="ltr" className="main-page__layout">
                    <Link to="/client/barcode-goods">
                        <CustomCard styles="main-page__scanner-card" img={ScannerIcon} text={translations.apiece.title} />
                    </Link>
                    <Link to="/client/weighted-goods">
                        <CustomCard styles="main-page__no-scanner-card" img={NoScannerIcon} text={translations.weighted.title} />
                    </Link>
                </Layout>
                <div dir="ltr" className="main-page__payment-info">
                    <span>Оплата проводиться тільки банківськими картками</span>
                    <CustomCard styles="main-page__visa-icon" img={VisaIcon} />
                    <CustomCard styles="main-page__mastercard-icon" img={MastercardIcon} />
                </div>
                <Footer />
            </div>
        );
    }
}

MainPage.propTypes = {
    direction: PropTypes.string.isRequired,
    translations: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    direction: directionSelectors.getDirection(state),
    activeLanguage: languagesSelectors.getActiveLanguage(state)
});

export default connect(mapStateToProps)(withLocalize(MainPage, translations));
