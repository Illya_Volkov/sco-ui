import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { directionSelectors } from 'data/direction';
import Keyboard from 'components/Keyboard';
import HelpAssistantModal from 'components/HelpAssistantModal';
import WeightedCategories from './WeightedCategories';
import WeightedCategoryItems from './WeightedCategoryItems';
import CustomButton from 'components/CustomButton';

import closeIcon from 'assets/close-small.svg';
import WeightedGoodsIcon from 'assets/weighted-goods.svg';
import RetailGoodsIcon from 'assets/retail-goods.svg';
// import Barcode from 'react-barcode';
import './WeightedGoods.scss';
class WeightedGoods extends React.Component {
    constructor(props) {
        super(props);
        this.inputRef = React.createRef();
    }

    state = {
        currentPage: 1,
        itemsPerPage: 9,
        pageItems: [],
        filteredItems: [],
        categoryItems: [],
        categories: [
            {
                title: 'Поштучні продукти',
                description: 'Хліб, печиво і т.д.',
                icon: RetailGoodsIcon,
                products: [
                    {
                        title: 'Булка ватрушка з сиром'
                    },
                    {
                        title: 'Батон пшеничний з кунжутом'
                    },
                    {
                        title: 'Батон Гірчичний'
                    },
                    {
                        title: 'Хліб білий'
                    },
                    {
                        title: 'Булка бублик в асортименті'
                    },
                    {
                        title: 'Мафін класичний'
                    },
                    {
                        title: 'Плетінка з шоколадом'
                    },
                    {
                        title: 'Пампух з заварним кремом '
                    },
                    {
                        title: 'Булочка альпійська'
                    },
                    {
                        title: 'Булочка житня'
                    },
                    {
                        title: 'Булочка домашня'
                    },
                    {
                        title: 'Булочка гречана'
                    },
                    {
                        title: 'Булочка здобна'
                    }
                ]
            },
            {
                title: 'Вагові продукти',
                description: 'Фрукті, овочі і т.д.',
                icon: WeightedGoodsIcon,
                products: [
                    {
                        title: 'Томати червоні'
                    },
                    {
                        title: 'Томати рожеві'
                    },
                    {
                        title: 'Томати бурі'
                    },
                    {
                        title: 'Яблука червоні'
                    },
                    {
                        title: 'Яблука зелені'
                    },
                    {
                        title: 'Мандарини'
                    },
                    {
                        title: 'Морква'
                    },
                    {
                        title: 'Картопля рожева'
                    },
                    {
                        title: 'Квртопля біла'
                    },
                    {
                        title: 'Буряк'
                    }
                ]
            }
        ],
        searchValue: '',
        showSearchInput: false,
        inputNode: null,
        keyboard: false,
        showHelpModal: false,
        showAssistantModal: false,
        showCategoryItems: false,
        prevArrowStyle: undefined,
        nextArrowStyle: undefined
    };

    render() {
        const {
            inputNode,
            searchValue,
            keyboard,
            showHelpModal,
            showAssistantModal,
            showCategoryItems,
            pageItems,
            prevArrowStyle,
            nextArrowStyle,
            showSearchInput,
            categories
        } = this.state;

        const { direction } = this.props;
        return (
            <div className="weighted-page">
                {showSearchInput && (
                    <div className="weighted-keyboard">
                        <label className={`weighted-keyboard__label weighted-keyboard__label--${direction === 'ltr' ? direction : 'rtl'}`}>
                            <input
                                className={`weighted-keyboard__input weighted-keyboard__input--${direction === 'ltr' ? direction : 'rtl'}`}
                                onInput={this.searchItems}
                                onChange={() => {}}
                                value={searchValue}
                                onFocus={this.handleFocus}
                                ref={this.inputRef}
                            />
                        </label>
                        <CustomButton
                            action={this.resetSearch}
                            text="Очистити"
                            styles={`weighted-keyboard__close-button weighted-keyboard__close-button--${direction === 'ltr' ? direction : 'rtl'}`}
                            icon={closeIcon}
                        />
                    </div>
                )}
                {!showCategoryItems && !searchValue ? (
                    <WeightedCategories
                        showKeyboard={this.showKeyboard}
                        showCategoryItems={this.showCategoryItems}
                        showSearchInput={showSearchInput}
                        categories={categories}
                    />
                ) : (
                    <WeightedCategoryItems
                        showKeyboard={this.showKeyboard}
                        hideCategoryItems={this.hideCategoryItems}
                        prevArrowStyle={prevArrowStyle}
                        nextArrowStyle={nextArrowStyle}
                        pageItems={pageItems}
                        prevPage={this.prevPage}
                        nextPage={this.nextPage}
                        showSearchInput={showSearchInput}
                        direction={direction}
                    />
                )}
                {keyboard && <Keyboard inputNode={inputNode} helpAssistant={this.openHelpModal} hideKeyboard={this.hideKeyboard} />}
                <HelpAssistantModal
                    closeHelpModal={this.closeHelpModal}
                    openAssistantModal={this.openAssistantModal}
                    closeAssistantModal={this.closeAssistantModal}
                    showHelpModal={showHelpModal}
                    showAssistantModal={showAssistantModal}
                />
            </div>
        );
    }

    prevPage = () => {
        const { currentPage, filteredItems, categoryItems, searchValue } = this.state;

        if (currentPage > 1) {
            this.setState(
                prev => ({ currentPage: prev.currentPage - 1 }),
                () => {
                    searchValue.length ? this.setPageItems(filteredItems) : this.setPageItems(categoryItems);
                }
            );
        }
    };

    nextPage = () => {
        const { currentPage, filteredItems, categoryItems, searchValue } = this.state;

        if (currentPage < this.getTotalPages()) {
            this.setState(
                prev => ({ currentPage: prev.currentPage + 1 }),
                () => {
                    searchValue.length ? this.setPageItems(filteredItems) : this.setPageItems(categoryItems);
                }
            );
        }
    };

    getTotalPages = () => {
        const { itemsPerPage, categoryItems } = this.state;

        return Math.ceil(categoryItems.length / itemsPerPage);
    };

    setPageItems = categoryItems => {
        const { itemsPerPage, currentPage } = this.state;
        const tempItems = [];

        for (let i = (currentPage - 1) * itemsPerPage; i < currentPage * itemsPerPage; i++) {
            if (typeof categoryItems[i] !== 'undefined') {
                tempItems.push(categoryItems[i]);
            }
        }

        this.setState({
            pageItems: tempItems
        });

        if (categoryItems.length < 9) {
            this.setState({
                prevArrowStyle: 'hidden',
                nextArrowStyle: 'hidden'
            });
        } else {
            currentPage === 1
                ? this.setState({
                      prevArrowStyle: 'hidden'
                  })
                : this.setState({
                      prevArrowStyle: 'visible'
                  });

            currentPage === this.getTotalPages()
                ? this.setState({
                      nextArrowStyle: 'hidden'
                  })
                : this.setState({
                      nextArrowStyle: 'visible'
                  });
        }
    };

    showCategoryItems = index => {
        const { categories } = this.state;

        this.setPageItems(categories[index].products);
        this.setState({
            categoryItems: categories[index].products,
            showCategoryItems: true
        });
    };

    hideCategoryItems = () => {
        this.setState({
            categoryItems: [],
            showCategoryItems: false
        });
    };

    searchItems = event => {
        const {
            target: { value }
        } = event;

        const { showCategoryItems, categories } = this.state;
        let tempItems;

        if (showCategoryItems) {
            const { categoryItems } = this.state;

            tempItems = categoryItems.filter(item => item.title.toLowerCase().search(value.toLowerCase()) !== -1);
        } else {
            let categoryItems = [];

            categories.map(item => categoryItems.push(...item.products));
            tempItems = categoryItems.filter(item => item.title.toLowerCase().search(value.toLowerCase()) !== -1);

            this.setState({ categoryItems });
        }

        this.setState({ searchValue: value, filteredItems: tempItems, currentPage: 1 }, () => {
            this.setPageItems(tempItems);
            this.handleFocus();
        });
    };

    resetSearch = () => {
        const { categoryItems } = this.state;

        this.setState({ searchValue: '', currentPage: 1, showSearchInput: false }, () => {
            this.setPageItems(categoryItems);
            this.hideKeyboard();
        });
    };

    handleFocus = () => this.setState({ inputNode: this.inputRef.current, keyboard: true }, () => this.inputRef.current.focus());

    showKeyboard = () => {
        this.setState(
            {
                keyboard: true,
                showSearchInput: true
            },
            () => {
                this.handleFocus();
            }
        );
    };

    hideKeyboard = () => this.setState({ inputNode: null, keyboard: false });

    openHelpModal = () => {
        this.setState({
            showHelpModal: true
        });
    };

    closeHelpModal = () => {
        this.setState({
            showHelpModal: false
        });
    };

    openAssistantModal = () => {
        this.setState({
            showHelpModal: false,
            showAssistantModal: true
        });
    };

    closeAssistantModal = () => {
        this.setState({
            showAssistantModal: false
        });
    };
}

WeightedGoods.propTypes = {
    direction: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
    direction: directionSelectors.getDirection(state)
});

export default connect(mapStateToProps)(WeightedGoods);
