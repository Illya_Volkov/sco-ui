import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'antd';

import CustomButton from 'components/CustomButton';
import searchIcon from 'assets/search-white.svg';

import './WeightedCategories.scss';
class WeightedCategories extends React.Component {
    render() {
        const { Meta } = Card;
        const { showKeyboard, showCategoryItems, showSearchInput, categories } = this.props;

        return (
            <div className="weighted-categories">
                {!showSearchInput && (
                    <div className="weighted-categories__header">
                        <span className="weighted-categories__header__title">Оберіть категорію</span>
                        <CustomButton
                            action={showKeyboard}
                            text="Пошук по всіх товарах"
                            styles="weighted-categories__search-button"
                            icon={searchIcon}
                        />
                    </div>
                )}

                <div dir="ltr" className="weighted-categories__body">
                    {categories.map((item, i) => (
                        <Card
                            key={i}
                            onClick={() => showCategoryItems(i)}
                            className="weighted-categories__card"
                            bordered={false}
                            cover={<img alt="" className="weighted-categories__card__retail-img" src={item.icon} />}
                        >
                            <Meta title={item.title} description={item.description} />
                        </Card>
                    ))}
                </div>
            </div>
        );
    }
}

WeightedCategories.propTypes = {
    showKeyboard: PropTypes.func.isRequired,
    showCategoryItems: PropTypes.func.isRequired,
    showSearchInput: PropTypes.bool.isRequired,
    categories: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default WeightedCategories;
