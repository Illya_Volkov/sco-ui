import React from 'react';
import PropTypes from 'prop-types';

import CustomButton from 'components/CustomButton';
import CustomCard from 'components/CustomCard';
import CustomIcon from 'components/CustomIcon';

import searchIcon from 'assets/search-white.svg';
import leftArrowSmallIcon from 'assets/left-arrow-small.svg';
import leftArrowIcon from 'assets/left-arrow.svg';
import rightArrowIcon from 'assets/right-arrow.svg';

import './WeightedCategoryItems.scss';
class WeightedCategoryItems extends React.Component {
    render() {
        const {
            showKeyboard,
            hideCategoryItems,
            prevArrowStyle,
            nextArrowStyle,
            pageItems,
            prevPage,
            nextPage,
            showSearchInput,
            direction
        } = this.props;

        return (
            <div className="category-items">
                {!showSearchInput && (
                    <div className="category-items__header">
                        <CustomButton
                            action={hideCategoryItems}
                            text="Назад до категорій"
                            styles={`category-items__back-button  category-items__back-button--${direction === 'ltr' ? direction : 'rtl'}`}
                            icon={leftArrowSmallIcon}
                        />
                        <CustomButton action={showKeyboard} text="Пошук по всіх товарах" styles="category-items__search-button" icon={searchIcon} />
                    </div>
                )}

                <div className="category-items__body">
                    <CustomIcon
                        styles={`category-items__prev-arrow category-items__prev-arrow--${prevArrowStyle} category-items__prev-arrow--${
                            direction === 'ltr' ? direction : 'rtl'
                        }`}
                        icon={leftArrowIcon}
                        action={prevPage}
                    />
                    {pageItems.map((item, i) => (
                        <CustomCard key={i} id="itemsTable" styles="category-items__card" text={item.title} />
                    ))}
                    <CustomIcon
                        styles={`category-items__next-arrow category-items__next-arrow--${nextArrowStyle} category-items__next-arrow--${
                            direction === 'ltr' ? direction : 'rtl'
                        }`}
                        icon={rightArrowIcon}
                        action={nextPage}
                    />
                </div>
            </div>
        );
    }
}

WeightedCategoryItems.propTypes = {
    direction: PropTypes.string.isRequired,
    hideCategoryItems: PropTypes.func.isRequired,
    nextPage: PropTypes.func.isRequired,
    prevPage: PropTypes.func.isRequired,
    showKeyboard: PropTypes.func.isRequired,
    showSearchInput: PropTypes.bool.isRequired,
    pageItems: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default WeightedCategoryItems;
