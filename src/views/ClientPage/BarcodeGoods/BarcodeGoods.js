import React from 'react';
import CustomCard from 'components/CustomCard';
import CustomButton from 'components/CustomButton';

import ScannerIcon from 'assets/scanner.svg';
import ScalesIcon from 'assets/scales.svg';

import './BarcodeGoods.scss';

class BarcodeGoods extends React.Component {
    render() {
        return (
            <div className="barcode-goods">
                <div className="barcode-goods__scanner-items">
                    <span className="barcode-goods__step">Крок 1</span>
                    <CustomCard styles="barcode-goods__scanner-card" img={ScannerIcon} text="Проскануйте товар" />
                    <CustomButton styles="barcode-goods__scanner-button" text="Ввести штрихкод вручну" />
                </div>
                <div className="barcode-goods__scales-items">
                    <span className="barcode-goods__step">Крок 2</span>
                    <CustomCard styles="barcode-goods__scales-card" img={ScalesIcon} text="Поставте товар на платформу" />
                </div>
            </div>
        );
    }
}

export default BarcodeGoods;
