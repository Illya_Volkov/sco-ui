import React from 'react';
import PropTypes from 'prop-types';

import CustomModal from 'components/CustomModal';

import scannerCardIcon from 'assets/scanner-card.svg';
import closeRedIcon from 'assets/close-red.svg';

import './ScanModal.scss';
class ScanModal extends React.Component {
    render() {
        const { showScanModal, closeScanModal } = this.props;

        return (
            <div>
                {showScanModal && (
                    <CustomModal
                        width={840}
                        height={580}
                        icon={scannerCardIcon}
                        closeIcon={closeRedIcon}
                        onCloseModal={closeScanModal}
                        onOkModal={closeScanModal}
                        styles="scan-modal"
                        title="Проскануйте вашу карту лояльності"
                        cancelText="Пропустити цей крок"
                        bodyText="Піднесіть картку штрихкодом до сканера"
                    />
                )}
            </div>
        );
    }
}

ScanModal.propTypes = {
    closeScanModal: PropTypes.func.isRequired,
    showScanModal: PropTypes.bool.isRequired,
};

export default ScanModal;
