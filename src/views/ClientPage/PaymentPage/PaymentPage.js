import React from 'react';
import { Link } from 'react-router-dom';
import { Layout } from 'antd';

import CustomCard from 'components/CustomCard';
import CustomButton from 'components/CustomButton';
import HelpAssistantModal from 'components/HelpAssistantModal';
import ScanModal from './ScanModal';

import assistantSmallIcon from 'assets/assistant-small.svg';
import BackArrowIcon from 'assets/left-arrow-small.svg';
import DollarBillIcon from 'assets/dollar-bill.svg';
import CreditCardIcon from 'assets/credit-card.svg';
import DiscountIcon from 'assets/discount.svg';

import './PaymentPage.scss';

class PaymentPage extends React.Component {
    state = {
        showHelpModal: false,
        showAssistantModal: false,
        showScanModal: false
    };
    render() {
        const { showHelpModal, showScanModal, showAssistantModal } = this.state;

        return (
            <div className="payment-page">
                <div className="payment-page__header">
                    <Link to="/client/barcode-goods">
                        <CustomButton action={this.back} text="Назад" styles="payment-page__back-button" icon={BackArrowIcon} />
                    </Link>

                    <div className="payment-page__title">Оберіть спосіб оплати</div>
                </div>
                <Layout className="payment-page__layout">
                    <Link to="/client/barcode-goods">
                        <CustomCard styles="payment-page__dollar-bill-card" img={DollarBillIcon} text="Готівка" />
                    </Link>
                    <Link to="/client/weighted-goods">
                        <CustomCard styles="payment-page__credit-card" img={CreditCardIcon} text="Картка" />
                    </Link>
                </Layout>
                <div className="payment-page__footer">
                    <CustomButton
                        action={this.openHelpModal}
                        text="Викликати асистента"
                        styles="payment-page__assistant-button"
                        icon={assistantSmallIcon}
                    />
                    <CustomButton
                        action={this.openScanModal}
                        text="У мене є картка лояльності"
                        styles="payment-page__discount-button"
                        icon={DiscountIcon}
                    />
                </div>

                <HelpAssistantModal
                    closeHelpModal={this.closeHelpModal}
                    openAssistantModal={this.openAssistantModal}
                    closeAssistantModal={this.closeAssistantModal}
                    showHelpModal={showHelpModal}
                    showAssistantModal={showAssistantModal}
                />
                <ScanModal closeScanModal={this.closeScanModal} showScanModal={showScanModal} />
            </div>
        );
    }

    back = () => {
        console.log('back');
    };

    openScanModal = () => {
        this.setState({
            showScanModal: true
        });
    };

    closeScanModal = () => {
        this.setState({
            showScanModal: false
        });
    };

    openHelpModal = () => {
        this.setState({
            showHelpModal: true
        });
    };

    closeHelpModal = () => {
        this.setState({
            showHelpModal: false
        });
    };

    openAssistantModal = () => {
        this.setState({
            showHelpModal: false,
            showAssistantModal: true
        });
    };

    closeAssistantModal = () => {
        this.setState({
            showAssistantModal: false
        });
    };
}

export default PaymentPage;
