import React from 'react';
// import { List } from 'antd';
import CustomIcon from 'components/CustomIcon';

import plusIcon from 'assets/plus.svg';
import minusIcon from 'assets/minus.svg';

import './SiderAddToReceipt.scss';

class SiderAddToReceipt extends React.Component {
    render() {
        const { receiptButtonStatus, totalSumStatus, initialReceiptCount, addProductItem, removeProductItem } = this.props;
        return (
            <div dir="ltr" className="receipt-menu">
                <div className="receipt-menu__header">Товар</div>
                {/* <div className="receipt-menu__weight-warning">Будь ласка, покладіть товар на сканер вагу та оберіть його</div> */}
                <div className="receipt-menu__selected-product">
                    <div className="receipt-menu__selected-product__title">Хліб білий</div>
                    <div className="receipt-menu__selected-product__counter">
                        <CustomIcon action={addProductItem} icon={plusIcon} styles="receipt-menu__selected-product__counter__button" />
                        <span className="receipt-menu__selected-product__counter__count">{initialReceiptCount}</span>
                        <CustomIcon action={removeProductItem} icon={minusIcon} styles="receipt-menu__selected-product__counter__button" />
                        <span className="receipt-menu__selected-product__counter__title">Кількість</span>
                    </div>
                    <div className="receipt-menu__selected-product__item-price">
                        <span>11.50 грн</span>
                        <span>Ціна за 1 шт</span>
                    </div>
                </div>
                <div className={`receipt-menu__total receipt-menu__total--${totalSumStatus}`}>
                    <span>23.00 грн</span>
                    <span>Ціна</span>
                </div>
                <div className={`receipt-menu__button receipt-menu__button--${receiptButtonStatus}`}>Додати до чеку</div>
            </div>
        );
    }
}

export default SiderAddToReceipt;
