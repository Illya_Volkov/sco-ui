import React from 'react';
import { List } from 'antd';

import './SiderReceipt.scss';

class SiderReceipt extends React.Component {
    render() {
        const { paymentButtonStatus, receiptList } = this.props;
        return (
            <div className="sider-list">
                <div className="sider-list__heder">Чек</div>
                <List
                    dataSource={receiptList}
                    renderItem={(item, index) => (
                        <List.Item dir="ltr" className="sider-list__item" key={index}>
                            <List.Item.Meta className="sider-list__item-index" title={`${index + 1}.`} />
                            <List.Item.Meta title={item.title} />
                            <div>{item.price}</div>
                        </List.Item>
                    )}
                />
                <div className="sider-list__total">
                    <span>Сума</span>
                    <span dir="ltr">74.25 грн</span>
                </div>
                <div className={`sider-list__button sider-list__button--${paymentButtonStatus}`}>Оплатити</div>
            </div>
        );
    }
}

export default SiderReceipt;
