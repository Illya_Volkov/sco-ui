import React from 'react';
import { Layout } from 'antd';

// import SiderReceipt from './SiderReceipt';
import SiderAddToReceipt from './SiderAddToReceipt';

import './SiderMenu.scss';

class SiderMenu extends React.Component {
    state = {
        initialReceiptCount: 1,
        paymentButtonStatus: 'active', //disabled
        receiptButtonStatus: 'active', //disabled
        totalSumStatus: 'visible', //hidden
        receiptList: [
            {
                title: 'Хліб білий 2 шт',
                price: '11.50 грн'
            },
            {
                title: 'Яблуко Голден 2.48 кг',
                price: '11.50 грн'
            },
            {
                title: 'Вода Моршинська слабогазована 1.5 Л',
                price: '11.50 грн'
            },
            {
                title: 'Хліб білий 2 шт',
                price: '11.50 грн'
            },
            {
                title: 'Яблуко Голден 2.48 кг',
                price: '11.50 грн'
            },
            {
                title: 'Вода Моршинська слабогазована 1.5 Л',
                price: '11.50 грн'
            },
            {
                title: 'Хліб білий 2 шт',
                price: '11.50 грн'
            },
            {
                title: 'Яблуко Голден 2.48 кг',
                price: '11.50 грн'
            },
            {
                title: 'Яблуко Голден 2.48 кг',
                price: '11.50 грн'
            },
            {
                title: 'Вода Моршинська слабогазована 1.5 Л',
                price: '11.50 грн'
            },
            {
                title: 'Хліб білий 2 шт',
                price: '11.50 грн'
            },
            {
                title: 'Яблуко Голден 2.48 кг',
                price: '11.50 грн'
            }
        ]
    };

    render() {
        const {
            // paymentButtonStatus, receiptList,
            receiptButtonStatus,
            initialReceiptCount,
            totalSumStatus
        } = this.state;
        return (
            <Layout.Sider width="370px" className="sider-menu">
                {/* <SiderReceipt paymentButtonStatus={paymentButtonStatus} receiptList={receiptList} /> */}
                <SiderAddToReceipt
                    receiptButtonStatus={receiptButtonStatus}
                    totalSumStatus={totalSumStatus}
                    initialReceiptCount={initialReceiptCount}
                    addProductItem={this.addProductItem}
                    removeProductItem={this.removeProductItem}
                />
            </Layout.Sider>
        );
    }

    addProductItem = () => this.setState(prev => ({ initialReceiptCount: prev.initialReceiptCount + 1 }));

    removeProductItem = () => {
        const { initialReceiptCount } = this.state;

        if (initialReceiptCount >= 2) {
            this.setState(prev => ({ initialReceiptCount: prev.initialReceiptCount - 1 }));
        }
    };
}

export default SiderMenu;
