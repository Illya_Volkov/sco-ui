import React from 'react';
import { Layout } from 'antd';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { directionActions, directionSelectors } from 'data/direction';
import { languagesActions, languagesSelectors } from 'data/languages';
import { withLocalize } from 'components/Localize';
import { translations } from 'translations/footer.translations';
import LanguageDropdown from './LanguageDropdown';
import CustomButton from 'components/CustomButton';
import HelpAssistantModal from 'components/HelpAssistantModal';

import assistantSmallIcon from 'assets/assistant-small.svg';
import muteIcon from 'assets/mute.svg';

import './Footer.scss';
class Footer extends React.Component {
    state = {
        showHelpModal: false,
        showAssistantModal: false,
        showLanguagesMenu: false,
        arrowIconTransform: 'no-rotate'
    };

    render() {
        const { showHelpModal, showAssistantModal, showLanguagesMenu, arrowIconTransform } = this.state;
        const {
            activeLanguage,
            languages,
            direction,
            translations
        } = this.props;

        return (
            <Layout.Footer className="footer">
                <CustomButton action={this.openHelpModal} text={translations.assistant} styles="footer__assistant-button" icon={assistantSmallIcon} />
                <CustomButton text={translations.volume} styles="footer__mute-button" icon={muteIcon} />
                <LanguageDropdown
                    showLanguages={this.showLanguages}
                    setActiveLanguage={this.setActiveLanguage}
                    showLanguagesMenu={showLanguagesMenu}
                    arrowIconTransform={arrowIconTransform}
                    activeLanguage={activeLanguage}
                    languages={languages}
                    direction={direction}
                />
                <HelpAssistantModal
                    closeHelpModal={this.closeHelpModal}
                    openAssistantModal={this.openAssistantModal}
                    closeAssistantModal={this.closeAssistantModal}
                    showHelpModal={showHelpModal}
                    showAssistantModal={showAssistantModal}
                    direction={direction}
                />
            </Layout.Footer>
        );
    }

    showLanguages = () => {
        const { showLanguagesMenu } = this.state;

        this.setState(
            {
                showLanguagesMenu: !showLanguagesMenu
            },
            () => {
                this.state.showLanguagesMenu
                    ? this.setState({
                          arrowIconTransform: 'rotate'
                      })
                    : this.setState({
                          arrowIconTransform: 'no-rotate'
                      });
            }
        );
    };

    setActiveLanguage = code => {
        const {
            setActiveLanguage,
            setDirection
        } = this.props;

        setActiveLanguage(code);
        this.showLanguages();
        code === 'ar' ? setDirection('rtl') : setDirection('ltr');
    };

    openHelpModal = () => {
        this.setState({
            showHelpModal: true
        });
    };

    closeHelpModal = () => {
        this.setState({
            showHelpModal: false
        });
    };

    openAssistantModal = () => {
        this.setState({
            showHelpModal: false,
            showAssistantModal: true
        });
    };

    closeAssistantModal = () => {
        this.setState({
            showAssistantModal: false
        });
    };
}

Footer.propTypes = {
    activeLanguage: PropTypes.string.isRequired,
    direction: PropTypes.string.isRequired,
    setActiveLanguage: PropTypes.func.isRequired,
    setDirection: PropTypes.func.isRequired,
    translations: PropTypes.object.isRequired,
    languages: PropTypes.arrayOf(PropTypes.object).isRequired
};

const mapStateToProps = state => ({
    direction: directionSelectors.getDirection(state),
    languages: languagesSelectors.getLanguages(state).languages,
    activeLanguage: languagesSelectors.getActiveLanguage(state)
});

const mapDispatchToProps = dispatch => {
    return {
        setDirection: direction => dispatch(directionActions.setDirection(direction)),
        setActiveLanguage: code => dispatch(languagesActions.setActiveLanguage(code))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withLocalize(Footer, translations));
