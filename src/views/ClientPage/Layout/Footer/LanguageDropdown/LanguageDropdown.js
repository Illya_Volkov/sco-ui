import React from 'react';
import CustomButton from 'components/CustomButton';
import PropTypes from 'prop-types';

import leftArrowSmallIcon from 'assets/left-arrow-small.svg';

import './LanguageDropdown.scss';

class LanguageDropdown extends React.Component {
    render() {
        const { activeLanguage, languages, direction, showLanguages, setActiveLanguage, showLanguagesMenu, arrowIconTransform } = this.props;

        return (
            <React.Fragment>
                {languages
                    .filter(item => item.code === activeLanguage)
                    .map(item => (
                        <CustomButton
                            text={item.name}
                            key={item.code}
                            styles={`language-button language-button--${direction === 'ltr' ? direction : 'rtl'}`}
                            icon={item.icon}
                        />
                    ))}
                {showLanguagesMenu && (
                    <div className="language-menu">
                        <ul className="language-menu__list">
                            {languages
                                .filter(item => item.code !== activeLanguage)
                                .map(item => (
                                    <li key={item.code} className="language-menu__item">
                                        <CustomButton
                                            action={() => setActiveLanguage(item.code)}
                                            text={item.name}
                                            styles="language-button"
                                            icon={item.icon}
                                        />
                                    </li>
                                ))}
                        </ul>
                    </div>
                )}
                <CustomButton
                    action={showLanguages}
                    icon={leftArrowSmallIcon}
                    styles={`arrow-language-button arrow-language-button--${arrowIconTransform} arrow-language-button--${
                        direction === 'ltr' ? direction : 'rtl'
                    }`}
                />
            </React.Fragment>
        );
    }
}

LanguageDropdown.propTypes = {
    activeLanguage: PropTypes.string.isRequired,
    languages: PropTypes.arrayOf(PropTypes.object).isRequired,
    direction: PropTypes.string.isRequired,
    showLanguages: PropTypes.func.isRequired,
    setActiveLanguage: PropTypes.func.isRequired,
    showLanguagesMenu: PropTypes.bool.isRequired,
    arrowIconTransform: PropTypes.string.isRequired
};

export default LanguageDropdown;
