import React from 'react';
import { NavLink } from 'react-router-dom';

import { Layout } from 'antd';

import './Header.scss';

class Header extends React.Component {
    render() {
        return (
            <Layout.Header dir="ltr" className="header">
                <NavLink to="/client/barcode-goods" activeClassName="header__active-tab">
                    <div className="header__tab-item">Сканувати штрихкод</div>
                    <span className="header__active-tab-cursor"/>
                </NavLink>
                <NavLink to="/client/weighted-goods" activeClassName="header__active-tab">
                    <div className="header__tab-item">Товари без штрихкоду</div>
                    <span className="header__active-tab-cursor"/>
                </NavLink>
            </Layout.Header>
        );
    }
}

export default Header;
